# Utilities for pygame

This is a collection of some extras for [pygame](https://www.pygame.org/): mainly focused on 2D vector graphics and visualising data.
Apart from pygame itself, it also depends on [NumPy](https://www.numpy.org/).

## `main`

Some useful constants for colours, fonts, etc. and functions for 2D calculations.

## `draw`

Functions to draw various things: vectors, plots, simple tables.

## `scene`

The beginnings of a somewhat higher-level library: draw a “scene” composed of polygons, with vector-based “sprites”: everything can be rotated, scaled, transformed. Also includes functions to detect collisions. (Note: as all the calculations and drawing is done in Python and with pygame in a fairly simplistic way, this is not very efficient, but might be good enough for implementing simple games or simulations, mainly for learning.)

## `images`

Functions to deal with images.
