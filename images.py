import numpy as np


_YUV_SDTV = np.array([[1.,                 0.,  0.701            ],
                      [1., -0.886*0.114/0.587, -0.701*0.299/0.587],
                      [1.,  0.886,                             0.]])
_YUV_SDTV[:,0]  *= 255./219.
_YUV_SDTV[:,1:] *= 255./112.
_YUV_SDTV = _YUV_SDTV.T

_YUV_HDTV = np.array([[1.164,     0.,  1.793],
                      [1.164, -0.213, -0.533],
                      [1.164,  2.112,     0.]]).T

def _clip_yuv(yuv):
    # clip input to the valid range
    ret = np.copy(yuv)
    ret[:,:,0] = yuv[:,:, 0].clip(16, 235).astype(yuv.dtype) - 16
    ret[:,:,1:] = yuv[:,:,1:].clip(16, 240).astype(yuv.dtype) - 128
    return ret

def grayscale224(im):
    """
    Convert an 8-bit image (e.g. a single channel) to a 24-bit grayscale image
    :param im: the input image (8-bit 2D array)
    :returns: grayscale image (24-bit 2D array)
    """
    return im[:,:] * (1+256+256*256)

def yuv2rgb_sdtv(im):
    """
    Convert an image from YUV to RGB using the "SDTV" (ITU-R BT.601) method.
    (Based on https://stackoverflow.com/questions/7041172/pils-colour-space-conversion-ycbcr-rgb and http://maxsharabayko.blogspot.com/2016/01/fast-yuv-to-rgb-conversion-in-python-3.html.)
    :param im: the image as a 3D numpy array (as used by picamera.array.PiYUVArray): [height][width][Y/U/V]
    :returns: an array with the converted 24-bit RGB image: [height][width][R/G/B]
    """
    return np.dot(_clip_yuv(im), _YUV_SDTV).clip(0, 255).astype('uint8')

def yuv2rgb_hdtv(im):
    """
    Convert an image from YUV to RGB using the "HDTV" (ITU-R BT.709) method.
    (Based on https://stackoverflow.com/questions/7041172/pils-colour-space-conversion-ycbcr-rgb and http://maxsharabayko.blogspot.com/2016/01/fast-yuv-to-rgb-conversion-in-python-3.html.)
    :param im: the image as a 3D numpy array (as used by picamera.array.PiYUVArray): [height][width][Y/U/V]
    :returns: an array with the converted 24-bit RGB image: [height][width][R/G/B]
    """
    return np.dot(_clip_yuv(im), _YUV_HDTV).clip(0, 255).astype('uint8')
