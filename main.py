import pygame
import numpy as np

BLACK = (0,0,0)
WHITE = (255,255,255)
GRAY = (128,128,128)
BLUE = (50,50,255)
RED = (255,0,0)
GREEN = (0,255,0)
YELLOW = (255,255,0)
ORANGE = (255,128,0)
LIGHT_GRAY = (128,128,128)
DARK_GRAY = (64,64,64)

FONT_SIZE = 15

CENTER = 'c'
LEFT = 'l'
RIGHT = 'r'
TOP = 't'
BOTTOM = 'b'
MIDDLE = 'm'

RAD_90 = np.pi / 2

pygame.init()

sans = pygame.font.SysFont("sans", FONT_SIZE)
mono = pygame.font.SysFont("monospace", FONT_SIZE+2, bold=True)

def pol2cart(v):
    """
    Convert polar coordinates to Cartesian.
    :param v: (length, angle in radians) tuple
    :returns: (x, y) tuple
    """
    (rho, phi) = v
    x = rho * np.cos(phi)
    y = rho * np.sin(phi)
    return (x, y)

def cart2len(p):
    """
    Return the length of a vector (hypotenuse) represented by Cartesian coordinates.
    :param p: (x,y) tuple of coordinates
    :return: the length of the vector 
    """
    (x, y) = p
    return np.sqrt(x**2 + y**2)

def cart2angle(p):
    """
    Return the angle of a vector (in radians) represented by Cartesian coordinates.
    :param p: (x,y) tuple of coordinates
    :return: the angle of the vector 
    """
    (x, y) = p
    return np.arctan2(y, x)

def text_pos(orig, text, font, align=(RIGHT, TOP), offset=(0,0)):
    """
    Calculate x,y position for the given text, relative to the given origin with the given alignment and offset.
    :param orig: the origin (as an (x,y) tuple)
    :param text: the text to render
    :param font: the font to use
    :param align: (horizontal_alignment, vertical_alignment): how the text is aligned relative to the origin
    :param offset: additional (x,y) offset
    :returns: (x,y) tuple that can be used with screen.blit() and font.render() to position the text
    """
    (halign, valign) = align
    if halign == RIGHT:
        x = 0
    elif halign == CENTER:
        x = -font.size(text)[0]/2
    elif halign == LEFT:
        x = -font.size(text)[0]

    if valign == TOP:
        y = 0
    elif valign == MIDDLE:
        y = font.size(text)[1]/2
    elif valign == BOTTOM:
        y = font.size(text)[1]

    return (orig[0] + x + offset[0], orig[1] + y + offset[1])

def rot_matrix(a):
    """
    Create a rotation matrix for the given angle.
    :param a: angle (in radians)
    :returns: a 3x3 (numpy) matrix
    """
    c, s = np.cos(a), np.sin(a)
    return np.array([
        [c, -s,  0],
        [s,  c,  0],
        [0,  0,  1]
    ])

def scale_matrix(sc):
    """
    Create a scale matrix for the given scale factor.
    :param sc: scale factor
    :returns: a 3x3 (numpy) matrix
    """
    return np.array([
        [sc, 0,  0],
        [0,  sc, 0],
        [0,  0,  1]
    ])

def transform(points, m):
    """
    Transform the given list of points using the given matrix
    :param points: list of points, e.g. [(x1, y1), (x2, y2), ...]
    :param m: transformation matrix (3x3)
    :returns: new list of points
    """
    return [(np.dot(m, np.append(p, 1)))[:-1] for p in points]

def move_and_rotate(points, pos, dir=0):
    """
    Move a polygon (list of points) to a given position and optionally rotate it first.
    :param pos: the position (points of the polygon are translated by this)
    :param points: the polygon
    :param dir: direction (rotation)
    """
    _pos = np.array(pos)
    if dir != 0:
        R = rot_matrix(dir)
        return [(np.dot(R, np.append(p, 1)))[:-1] + _pos for p in points]
    else:
        return [np.array(p) + _pos for p in points]

def _perp(a):
    b = np.empty_like(a)
    b[0] = -a[1]
    b[1] = a[0]
    return b

def point_in_seg(seg, p):
    (a, b) = seg
    return (a[0] <= p[0] <= b[0] or b[0] <= p[0] <= a[0]) and (a[1] <= p[1] <= b[1] or b[1] <= p[1] <= a[1])

def seg_intersect_point(seg1, seg2):
    """
    Check whether two line segments intersect (including overlap when they are colinear) and return the intersection point.
    This is less efficient than seg_intersect if only a check is needed without knowing the intersection point.
    (Based on https://stackoverflow.com/questions/3252194/numpy-and-line-intersections)
    :param seg1: first line segment ((x1,y1), (x2,y2))
    :param seg2: second line segment ((x1,y1), (x2,y2))
    :returns: the intersection point or None if no intersection
    """
    (a1,a2) = seg1
    (b1,b2) = seg2

    da = a2 - a1
    db = b2 - b1
    dp = a1 - b1
    dap = _perp(da)
    denom = np.dot(dap, db)
    num = np.dot(dap, dp)
    if denom == 0:  # parallel: check if any of the end points are on the other segment
        if point_in_seg(seg2, a1):
            return a1
        elif point_in_seg(seg2, a2):
            return a2
        elif point_in_seg(seg1, b1):
            return b1
        elif point_in_seg(seg1, b2):
            return b2
        else:
            return None
    p = (num / denom.astype(float)) * db + b1
    return p if point_in_seg(seg1, p) and point_in_seg(seg2, p) else None

def orientation(p, q, r):
    """
    Find orientation of ordered triplet (p, q, r).
    The function returns following values
    0 --> p, q and r are colinear
    1 --> Clockwise
    2 --> Counterclockwise
    (Copied from https://stackoverflow.com/questions/3838329/how-can-i-check-if-two-segments-intersect)
    """
    val = ((q[1] - p[1]) * (r[0] - q[0]) - (q[0] - p[0]) * (r[1] - q[1]))
    if val == 0:
        return 0  # colinear
    elif val > 0:
        return 1  # clockwise
    else:
        return 2  # counter-clockwise

def seg_intersect(seg1, seg2):
    """
    Check whether the the given closed line segments intersect.
    (Copied from https://stackoverflow.com/questions/3838329/how-can-i-check-if-two-segments-intersect)
    """
    (p1, q1) = seg1
    (p2, q2) = seg2
    o1 = orientation(p1, q1, p2)
    o2 = orientation(p1, q1, q2)
    o3 = orientation(p2, q2, p1)
    o4 = orientation(p2, q2, q1)

    # General case
    if (o1 != o2 and o3 != o4):
        return True

    # Special Cases
    # p1, q1 and p2 are colinear and p2 lies on segment p1q1
    if (o1 == 0 and point_in_seg(seg1, p2)):
        return True

    # p1, q1 and p2 are colinear and q2 lies on segment p1q1
    if (o2 == 0 and point_in_seg(seg1, q2)):
        return True

    # p2, q2 and p1 are colinear and p1 lies on segment p2q2
    if (o3 == 0 and point_in_seg(seg2, p1)):
        return True

    # p2, q2 and q1 are colinear and q1 lies on segment p2q2
    if (o4 == 0 and point_in_seg(seg2, q1)):
        return True

    return False # Doesn't fall in any of the above cases

