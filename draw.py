from .main import *

def vect(screen, orig, v, width=1, color=GREEN, font=None, text_align=(CENTER, BOTTOM), text_offset=(0,0), vertical=False, text=None):
    """
    Draw a vector (line segment) with an optional label showing the angle.
    :param screen: the screen
    :param orig: the origin (as an (x,y) tuple)
    :param v: the vector (as a (length,angle) tuple with polar coordinates using radians)
    :param width: line width
    :param color: line color
    :param font: font to use to render the label (no label if font is None, the default)
    :param text_align: label alignment relative to orig (see text_pos())
    :param text_offset: additional (x,y) offset for the label (see text_pos())
    :param vertical: if True, treat 0 degrees as pointing upwards on the Y axis (that is, rotate the given angle by -90 degrees)
    :param text: if set, render this instead of the angle
    """

    if vertical:
        x, y = pol2cart((v[0], v[1] - RAD_90))  # rotate 90 deg left for drawing
    else:
        x, y = pol2cart(v)
    pygame.draw.line(screen, color, orig, (x + orig[0], y + orig[1]), width)
    
    if font:
        _text = text if text is not None else '{:>3.0f}'.format(np.rad2deg(v[1]))
        screen.blit(font.render(_text, 1, color), text_pos(orig, _text, font, align=text_align, offset=text_offset))

def plot(screen, rect, vrange, values, color=WHITE, border_color=None, show_current_value=False, current_value_offset=(0,0), title=None):
    """
    Plot the given list of values as a simple graph in the given rectangle.
    The X axis is not scaled: one value will use one pixel. If the list of values is short, it won't fill the rectangle.
    If there are more values than the available width, values will be rendered from the end of the list on the right, until
    there isn't more space left. (Values at the end are expected to be most recent data, so this is useful for plotting
    some value in real time with a limited "history".)
    :param screen: the screen
    :param rect: the rectangle for the plot
    :param vrange: a tuple with the (min, max) values for the values (so that values can be scaled properly when plotting)
    :param values: list of values to plot
    :param color: color for the plot
    :param border_color: color for an optional border
    :param show_current_value: show the current value on the right
    :param current_value_offset: offset for the current value position
    :param title: title to show below the plot
    """
    if border_color:
        pygame.draw.rect(screen, border_color, rect, 1)
        # add some margins around the plot inside the border:
        rect = rect.inflate(-8, -8)
    if title:
        screen.blit(sans.render(title, 1, border_color or GRAY), (rect.x-4, rect.y + rect.h + 8))
    
    sc = (rect.h - 1) / abs(vrange[1] - vrange[0])  # value scaling factor

    # show the most recent values (end of the array) if not everything fits the rect:
    if rect.w > len(values):
        xstart, xend = 0, len(values)
    else:
        xstart, xend = len(values) - rect.w, len(values)

    # first value (invert value and shift and scale to fit into rect)
    value = round((vrange[1] - values[xstart]) * sc) if xstart < len(values) else 0
    for x in range(xstart, xend):
        p1 = (rect.x + x - xstart, rect.y + value)
        value = round((vrange[1] - values[x]) * sc)  # invert value and shift and scale to fit into rect
        p2 = (rect.x + x - xstart, rect.y + value)
        pygame.draw.line(screen, color, p1, p2)

    if show_current_value and len(values) > 0:
        screen.blit(sans.render('{}'.format(values[-1]), 1, color), (rect.x + rect.w + 10 + current_value_offset[0], rect.y + current_value_offset[1]))
        
def table(screen, pos, data, cols = [], rows = [], hgap = 0, emhgap = 0, vgap = 0, hpad = 0, vpad = 0, align = LEFT, color = WHITE, background = None):
    """
    Draw a simple table: render a given 2-dimensional array of text, automatically sizing columns to fit the contents.
    :param screen: the screen
    :param pos: the top left corner (as an (x,y) tuple)
    :param data: the data to render (2 dimensional array of text)
    :param cols: optional formatting for columns: list of dicts with optional "font", "color", "background", "align", "hgap", "emhgap", "width", "emwidth" keys
    :param hgap: horizontal gap between columns
    :param emhgap: horizontal gap between columns, in "ems"
    :param vgap: vertical gap between columns
    :param hpad: horizontal padding in columns
    :param vpad: vertical padding in columns
    :param align: horizontal alignment in cells
    :param color: default color for the whole table
    :param background: default background for the whole table

    """
    if len(data) == 0:
        return
    def get_font(i):
        return sans if len(cols) <= i else cols[i].get('font', sans)
    def get_color(row, col):
        c = color if len(cols) <= col else cols[col].get('color', color)
        return c if len(rows) <= row else rows[row].get('color', c)
    def get_background(row, col):
        b = background if len(cols) <= col else cols[col].get('background', background)
        return b if len(rows) <= row else rows[row].get('background', b)
    def get_align(i):
        return align if len(cols) <= i else cols[i].get('align', align)

    first_emwidth = get_font(0).size('m')[0]

    def get_hgap(i):
        if i == len(cols) - 1:
            return 0
        elif len(cols) > i:
            _hgap = cols[i].get('hgap', hgap)
            _emhgap = cols[i].get('emhgap', emhgap)
        else:
            _hgap = hgap
            _emhgap = emhgap
        return _emhgap * first_emwidth if _emhgap != 0 else _hgap
    def get_hpad(i):
        return hpad if len(cols) <= i else cols[i].get('hpad', hpad)

    # calculate column sizes from data:
    col_w = [get_font(i).size(text)[0] + get_hgap(i) + get_hpad(i) * 2 for i, text in enumerate(data[0])]
    for i, row in enumerate(data[1:]):
        col_w = np.maximum(col_w, [get_font(i).size(text)[0] + get_hgap(i) + get_hpad(i) * 2 for i, text in enumerate(row)])
    # override where size is explicitly set:
    for i, col in enumerate(cols):
        emwidth = cols[i].get('emwidth', None)
        if emwidth is not None:
            col_w[i] = get_font(i).size('m')[0] * emwidth
        else:
            col_w[i] = cols[i].get('width', col_w[i])
    
    y = pos[1]
    linesize = 0
    for yi, row in enumerate(data):
        x = pos[0]
        for xi, text in enumerate(row):
            font = get_font(xi)
            _align = get_align(xi)
            size = font.size(text)
            linesize = font.get_linesize()
            fontheight = font.get_height()
            bg = get_background(yi, xi)
            _hgap = get_hgap(xi)
            _hpad = get_hpad(xi)
            if bg is not None:
                w = col_w[xi]
                h = linesize + vpad*2 + 1
                pygame.draw.rect(screen, bg, ((x, y), (w - _hgap, h - vgap)))
            xpos = x + col_w[xi] - size[0] - _hgap - _hpad if _align == RIGHT else x + _hpad
            screen.blit(font.render(text, 1, get_color(yi, xi)), (xpos, y  + (linesize - fontheight + vgap + vpad * 2)/2))
            x += col_w[xi]
        y += linesize + vgap + vpad * 2
        
