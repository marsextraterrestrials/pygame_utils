import numpy as np
import math
import pygame
from .main import *

def find_hit(lines, beam):    
    min_len = math.inf
    for line in lines:
        p = seg_intersect_point(beam, line)
        if not p is None:
            l = cart2len(p - beam[0])
            if l < min_len:
                min_len = l
    if min_len != math.inf:
        beam_hit = beam[0] + pol2cart((min_len, cart2angle(beam[1] - beam[0])))
        return beam_hit

def find_poly_hits(polys, beam):
    min_len = math.inf
    for poly in polys:
        points = poly.points
        for i in range(0, len(points)-1):
            line = np.array((points[i], points[i+1]))
            p = seg_intersect_point(beam, line)
            if not p is None:
                l = cart2len(p - beam[0])
                if l < min_len:
                    min_len = l
            # also join last pt to first:
            if poly.closed:
                p = seg_intersect_point(beam, np.array((points[0], points[-1])))
                if not p is None:
                    l = cart2len(p - beam[0])
                    if l < min_len:
                        min_len = l
            
    if min_len != math.inf:
        beam_hit = beam[0] + pol2cart((min_len, cart2angle(beam[1] - beam[0])))
        return (min_len, beam_hit)

def polys_intersect(polys, seg):
    for poly in polys:
        points = poly.points
        for i in range(0, len(points)-1):
            line = np.array((points[i], points[i+1]))
            if seg_intersect(seg, line):
                return True
            # also join last pt to first:
            if poly.closed:
                if seg_intersect(seg, np.array((points[0], points[-1]))):
                    return True
    return False
    
    
class SimplePoly:
    def __init__(self, points, color = GRAY, width = 1, closed = True):
        self.points = points
        self.color = color
        self.width = width
        self.closed = closed

class VectorSprite:
    """
    A simple vector sprite that has a single polygon
    """
    def __init__(self, points, color = None, width = 1):
        self.orig_points = points
        self.points = points
        self.color = color
        self.width = width
        self.closed = True
        self.pos = (0, 0)
        self.reset()

    def _tr(self):
        return transform(self.orig_points, self.tr) if self.tr is not None else self.orig_points
        
    def _update_points(self):
        self.points = (self._tr()) + self.pos

    def move(self, pos):
        self.pos = np.array(pos)
        self._update_points()

    def translate(self, tr):
        self.pos = self.pos + np.array(tr)
        self._update_points()

    def scale(self, sc):
        self.sc = scale_matrix(sc) if sc else None
        self.tr = self.rot @ self.sc if self.rot is not None else self.sc
        self._update_points()
        
    def rotate(self, a):
        self.rot = rot_matrix(a) if a else None
        self.tr = self.rot @ self.sc if self.sc is not None else self.rot
        self._update_points()
        
    def reset(self):
        self.points = self.orig_points
        self.pos = np.array((0, 0))
        self.sc = None
        self.rot = None
        self.tr = None
        
        
class VectorSpriteGroup:
    """
    A vector sprite built from multiple VectorSprite components.
    When the group is moved, rotated or scaled, the components are changed together. When the components are moved, rotated or scaled separately,
    this is interpreted relative to the group and these relative transformations are preserved when the group itself is transformed.
    """
    def __init__(self, sprites):
        self.sprites = sprites
        self.sc = None
        self.rot = None
        self.tr = None
        self.pos = np.array((0, 0))

    def _update_sprite_points(self):
        for s in self.sprites:
            s.points = (transform(s.orig_points, s.tr) if s.tr is not None else s.orig_points) + s.pos
            if self.tr is not None:
                s.points = transform(s.points, self.tr)
            s.points = s.points + self.pos
        
    def rotate(self, a):
        self.rot = rot_matrix(a) if a else None
        self.tr = self.rot @ self.sc if self.sc is not None else self.rot
        self._update_sprite_points()
        
    def scale(self, sc):
        self.sc = scale_matrix(sc) if sc else None
        self.tr = self.rot @ self.sc if self.rot is not None else self.sc
        self._update_sprite_points()
        
    def move(self, pos):
        self.pos = pos
        self._update_sprite_points()

    def translate(self, tr):
        self.pos = self.pos + np.array(tr)
        self._update_sprite_points()

    #TODO this is messy and only used for collision - do something better!
    def tr_sprite(self, s, a, _tr):
        if a is not None:
            rot = self.rot @ rot_matrix(a) if self.rot is not None else rot_matrix(a)
            tr = rot @ self.sc if self.sc is not None else rot
        else:
            tr = self.rot @ self.sc if self.rot is not None else self.sc
        points = (transform(s.orig_points, s.tr) if s.tr is not None else s.orig_points) + s.pos
        if tr is not None:
            points = transform(points, tr)
        points = points + self.pos + _tr
        return points
        
        
class Scene:
    def __init__(self, screen, scale = 1, translate = np.array((0, 0))):
        self.scale = scale
        self.translate = translate
        self.A = np.array([
            [scale, 0,     translate[0]],
            [0,     scale, translate[1]],
            [0,     0,     1]
        ])
        self.screen = screen
        self.polys = []

    def _tr(self, p):
        """
        Apply the A affine transform to p
        """
        return (self.A @ np.append(p, 1))[:-1]

    def _draw_poly(self, poly):
        """
        Draw a polygon on the scene.
        :param poly: the polygon (see class documentation about the expected structure)
        """
        if poly.color is not None:
            if poly.closed:
                pygame.draw.polygon(self.screen, poly.color, [self._tr(p) for p in poly.points], poly.width)
            else:
                pygame.draw.lines(self.screen, poly.color, False, [self._tr(p) for p in poly.points], poly.width)

    def add(self, poly):
        self.polys.append(poly)
        
    def draw(self):
        for poly in self.polys:
            self._draw_poly(poly)

    def zoom(self, scale):
        self.scale = scale
        self.A = np.array([
            [scale, 0,        self.translate[0]],
            [0,        scale, self.translate[1]],
            [0,        0,        1]
        ])
        
